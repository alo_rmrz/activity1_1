defmodule Exercises do
  def occurs(list, word) do
    Enum.count(list, fn item -> item == word end)
  end

  def is_sorted (list) do
    if list == Enum.sort(list) do
      true
    else
      false
    end
  end

  def unique(list) do
    Enum.uniq(list)
  end
end
