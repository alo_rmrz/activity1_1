defmodule ExercisesTest do
  use ExUnit.Case
  doctest Exercises

  test "occurs: counts the number of occurrences of a word in a list" do
    assert Exercises.occurs(["hi","hola","hi","hello","hoi"], "hej") == 0
    assert Exercises.occurs(["hi","hola","hi","hello","hoi"], "hola") == 1
    assert Exercises.occurs(["hi","hola","hi","hello","hoi"], "hi") == 2
  end

  test "is_sorted" do
    assert Exercises.is_sorted([3, 13, 27, 771, 674, 301]) == false
    assert Exercises.is_sorted([3, 13, 27, 71, 674, 1301])
    assert Exercises.is_sorted(Enum.take(1..20, :rand.uniform(20)))
    assert Exercises.is_sorted([100 | Enum.take(1..20, :rand.uniform(20))]) == false
  end

  test "unique: should work with list of numbers" do
    assert Exercises.unique([2,2,2,1,2,2,5,5,5,5,7,7]) == [2,1,5,7]
    assert Exercises.unique([1,2,2,2,2,2,5,5,5,5,7,7]) == [1,2,5,7]
    assert Exercises.unique([1,2,2,1,2,2,2,5,5,5,5,7,7]) == [1,2,5,7]
  end

  test "unique: should work with list of strings" do
    assert Exercises.unique(~w[a a a a b c c a a d e e e e]) == ["a","b","c","d","e"]
  end

  test "unique: should work with charlists" do
    assert Exercises.unique('bccaadeeeeaaaa') == 'bcade'
  end
end
